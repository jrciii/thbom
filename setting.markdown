---
title: Setting
---

World: Karnorion

[![][karnorion]][karnorion]


Current city: Mintana

[![][mintana]][mintana]


Caravan route:

[![][caravan-route]][caravan-route]


[karnorion]:        images/karnorion.png { width=75% }
[mintana]:          images/mintana_thbom.png { width=75% }
[caravan-route]:    images/caravan_route.png { width=75% }
